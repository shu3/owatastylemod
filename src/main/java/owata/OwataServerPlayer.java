package owata;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.util.DamageSource;
import api.player.server.ServerPlayerAPI;
import api.player.server.ServerPlayerBase;

/** server side extended player **/
public class OwataServerPlayer extends ServerPlayerBase {
	public OwataServerPlayer(ServerPlayerAPI playerAPI) {
		super(playerAPI);
		
		player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(0.1D);
		player.setHealth(0.1f);
	}

	@Override
	public boolean attackEntityFrom(DamageSource dmgsrc, float amount)
	{
		if (player.capabilities.disableDamage)
		{
			return false;
		}
		if (player.isEntityAlive())
		{
			player.func_110142_aN().func_94547_a(dmgsrc, player.getHealth(), player.getHealth());
			player.setHealth(0);
			player.onDeath(dmgsrc);
			player.setDead();
			if (player.isPlayerSleeping() && !player.worldObj.isRemote)
			{
				player.wakeUpPlayer(true, true, false);
			}
			return false;
		}

		return super.attackEntityFrom(dmgsrc, amount);
	}
}
