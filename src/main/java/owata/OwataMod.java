package owata;

import api.player.client.ClientPlayerAPI;
import api.player.server.ServerPlayerAPI;
import net.minecraft.init.Blocks;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.relauncher.Side;

@Mod(modid = OwataMod.MODID, version = OwataMod.VERSION)
public class OwataMod
{
    public static final String MODID = "owata";
    public static final String VERSION = "1.0";
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    }
    
    @EventHandler
    public void load(FMLInitializationEvent event) {
    	System.out.println("!! Load server player !!");
    	ServerPlayerAPI.register(MODID, OwataServerPlayer.class);

    	if (event.getSide() == Side.CLIENT) {
        	System.out.println("!! Load client player !!");
    		ClientPlayerAPI.register(MODID, OwataClientPlayer.class);
    	}
    }
}
