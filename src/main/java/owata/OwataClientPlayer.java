package owata;

import net.minecraft.util.DamageSource;
import api.player.client.ClientPlayerAPI;
import api.player.client.ClientPlayerBase;

/** client side extended player **/
public class OwataClientPlayer extends ClientPlayerBase {

	public OwataClientPlayer(ClientPlayerAPI playerAPI) {
		super(playerAPI);
	}

	@Override
	public boolean attackEntityFrom(DamageSource dmgsrc, float amount)
	{
		return false;
	}

}
